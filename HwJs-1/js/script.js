// let a = 0,
//     isTrue = true;
// isTrue && (a = 555);
// console.log(a);
//
// let someString = "777";
// let newString = someString || "Default String";
// console.log(newString);

/* TASK - 1
* Get any integer number from user.
* After you get the number, show modal window with message about is number even or odd.
*/


// let numUser = prompt ("введите число")
// if (isNaN(numUser)) {
// alert("не число")
// }
// else if (numUser % 2 === 0) {
//     alert("четное")
// }
// else {
//     alert("нечетное")
// }

/* TASK - 2
* Ask user in witch language he wants to see the list of the days.
* User should enter one of three values, they are - urk, en, ru
* Show the list of days on selected language.
* */

// const rus = "пн, вт, ср"
// const eng = "mo, tu, we"
// var userAms = +prompt("введите язык: 1 - русский, 2 - английский")
// if (isNaN(userAms)) {
//     alert("неверный ввод")
// }
// else if (userAms > 2) {
//     alert("неверный ввод")
//     prompt("введите язык: 1 - русский, 2 - английский")
// }
// else if (userAms === 1) {
//     alert(rus)
// }
// else if (userAms === 2) {
//     alert(eng)
// }


/* TASK - 5
* Write a coffee-machine program.
* Program can accept coins and prepare drinks
* (Coffee - 30 UAH, Cappuccino - 40 UAH, special extra vegan no gluten herbal tea - 200 UAH).
*
* It means that the user enters the amount of money that he gives in the modal window,
* next - he enters the name of the drink he wants.
*
* Depending of the drink, you need to calculate the change and show the message:
* `Your drink *DRINK_NAME* and change *CHANGE*`
* If the change equals to 0, show next message:
* `Your drink *DRINK_NAME. Thank you for the exact amount of money!*`
* */

// const userMoney = +prompt("Кофе - 30 грн, Каппучино - 50 грн. ДАЙТЕ ДЕНЕГ")
// if (isNaN(userMoney)) {
//     alert("это не деньги")
// } else if (userMoney < 30) {
//     alert("недостаточно денег")
// }
// else if (userMoney < 50) {
//     alert("ваш кофе готовится")
// } else if (userMoney >= 50) {
//     alert("ваш Капуччино готовится")
// }

// let i = 0;
// while (i < 3) {
//     alert (i);
//     i++;
// }


// for (инициализация;тест;инкремент) тело цикла


// for (let i=0;i<20;i++) {
//     console.log(i);
// }

// var i;
// for (i=10;i--;) {
//     console.log(i);
// }

// var i=100;
// while (i--) {console.log(i);}

// var i=10;
// do {
//     console.log(i--)
// } while (i>=0)


// ДОМАШКА - 1


let userName = prompt("Enter your NAME");
while (Boolean(userName) === false || !isNaN(userName)) {
    userName = prompt("Wrong data. Enter your NAME again!", "")

}
let userAge = +prompt("Enter your AGE");
while (Boolean(userAge) === false || isNaN(userAge)) {
    userAge = +prompt("Wrong data. Enter your AGE again!")
}

if (userAge < 18) {
    alert("You are not allowed to visit this website")
} else if (userAge >= 18 && userAge <= 22) {
    let userAnswer = confirm("Are you sure you want to continue?")
    if (userAnswer === true) {
        alert("Welcome" + " " + userName + "!");
    } else {
        alert("You are not allowed to visit this website")
    }
} else if (userAge > 22) {
    alert("Welcome" + " " + userName + "!")
}


// debugger
// for (let i = 0; i < 1000; i+=200) {
//     console.log(i)
// }

// let name = prompt("Whats your NAME?")
// while (!isNaN(name)) {
//    name = prompt("Wrong data. Enter your NAME again!")
// }
// let secondName = prompt("Whats your SECOND NAME?")
// while (!isNaN(secondName)) {
//     secondName = prompt("Wrong data. Enter your SECOND NAME again!")
// }
// let age = prompt("Whats your AGE?")
// while (isNaN(age) || age <= 0) {
//     age = prompt("Wrong data. Enter your AGE again!")
// }
// alert("Welcome" + " " + name + " " + secondName + " " + "в возрасте" + " " + age + " " + "лет" + "!")


// for (let i = 1; i < 149; i +=2) {
//     if (i % 2 !== 0)
//     {
//         console.log(i)
//     }
// }


// let num1 = prompt("Enter NUM1")
// while (isNaN(num1)) {
//    num1 = prompt("Wrong data. Enter NUM1 again!")
// }
//
// let action = prompt("Enter ACTION")
// while (action !== "-" && action !== "+" && action !== "*" && action !== "/") {
//     action = prompt("Wrong data. Enter ACTION again!")
// }
//
// let num2 = prompt("Enter NUM2")
// while (isNaN(num2)) {
//     num2 = prompt("Wrong data. Enter NUM2 again!")
// }
//
// alert(num1 + action + num2)


//     function func() {
//
//         var num1 = Number(prompt("введите число 1"));
//         var op = prompt("введите оператор");
//         var num2 = Number(prompt("введите число 2"));
//
//     switch (op !== "-" && op !== "+" && op !== "*" && op !== "/") {
//     case '+':
//     result = num1 + num2;
//     break;
//     case '-':
//     result = num1 - num2;
//     break;
//     case '*':
//     result = num1 * num2;
//     break;
//     case '/':
//     if (num2) {
//     result = num1 / num2;
// } else {
//     result = 'бесконечность';
// }
//     break;
//     default:
//     result = 'выберите операцию';
// }
//         alert(result);
// }
//
// alert(func())

// Task-1
// function sum(a,b) {
//     return (a + b)
// }
// console.log(sum(2,3))


/* ЗАДАНИЕ - 2
* Написать функцию которая будет принимать два аргумента - число с которого начать отсчет и число до которого нужно досчитать.
* Под отсчетом имеется в виду последовательный вывод чисел в консоль с увеличением на единицу.
*/

// function count(from, to) {
//     for (let i = from; i <= to; i++) {
//         console.log(i)
//     }
// }
//
// count(4, 20)




/* ЗАДАНИЕ - 3
* Написать функцию, которая будет суммировать ВСЕ числа, которые будут переданы ей в качестве аргументов.
* */

/* ЗАДАНИЕ - 5
* Написать функцию getMultipleOf, которая будет выводить в консоль ВСЕ числа в заданом диапазоне, которые кратны третьему числу.
* Аргументы функции:
*   1) число С которого начинается диапазон (включительно)
*   2) число которым диапазон заканчивается (включительно)
*   3) число КРАТНЫЕ КОТОРОМУ нужно вывести в консоль
* Возвращаемое значение - отсутствует
* */

// function getMultipleOf (start, end) {
//     for (let result = start; result <=end; result++)
//         if (result % 3 == 0) console.log(result)
//
// }
// getMultipleOf(1, 30)

function getMultipleOf(from, to, multiple) {
    for (from; from <= to; from++) {
        if (from % multiple == 0) console.log(from)
    }
}

getMultipleOf(1,30,10)